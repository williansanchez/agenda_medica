const Joi = require('@hapi/joi')

const agendaSchema = Joi.object({
  title: Joi.string().min(2).max(50).optional(),
  description: Joi.string().min(20).max(200).optional(),
  patient: Joi.string().min(3).max(100).optional(),
  doctor: Joi.string().min(3).max(100).optional(),
  date: Joi.string().min(6).max(30).optional(),
  notification: Joi.string().min(5).max(50).optional()
})

const agendaSchemaRequired = Joi.object({
  title: Joi.string().min(2).max(50).required(),
  description: Joi.string().min(20).max(200).required(),
  patient: Joi.string().min(3).max(100).required(),
  doctor: Joi.string().min(3).max(100).required(),
  date: Joi.string().min(6).max(30).required(),
  notification: Joi.string().min(5).max(50).required()
})

module.exports = {
  agendaSchema,
  agendaSchemaRequired
}
