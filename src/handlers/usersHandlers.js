const bcrypt = require('bcrypt')
const boom = require('@hapi/boom')

const User = require('../models/User')

const generateToken = require('../utils/generateToken')
const convertToJsonSpecUser = require('../utils/convertToJsonSpecUser')

module.exports = {
  async register (request, h) {
    try {
      const { username, email, typeuser } = request.payload

      const userExists = await User.findOne({ username })

      if (userExists) {
        const errorMessage = 'This user already exists, try other username'
        return boom.conflict(errorMessage)
      }

      const emailUser = await User.findOne({ email })

      if (emailUser) {
      const errorMessage = 'The Email is already in use.'
      return boom.conflict(errorMessage)
      }
    /*  
      if (typeuser!= 'patient' || typeuser!= 'doctor') {
        const errorMessage = 'A "patient" or "doctor" must enter in typeuser.'
        return boom.conflict(errorMessage)
      }
*/
      const user = await User.create(request.payload)

      const token = generateToken(user)
      const data = convertToJsonSpecUser(user)
      const dataUser = {
        data
      }
      const response = h.response({ success: true, dataUser })
      response.header('Authorization', request.headers.authorization)

      return response.code(201)
    } catch (err) {
      const errorMessage = 'Could not create a new account'
      return boom.badRequest(errorMessage)
    }
  },

  async login (request, h) {
    const errorMessage = 'Authentication failed'
    try {
      const { username, password } = request.payload

      const user = await User.findOne({ username })
      if (!user) {
        return boom.unauthorized(errorMessage)
      }

      if (!await bcrypt.compare(password, user.password)) {
        return boom.unauthorized(errorMessage)
      }

      const token = generateToken(user)
      const data = convertToJsonSpecUser(user)
      const dataUser = {
        data
      }
      const response = h.response({ success: true, token, dataUser })
      response.header('Authorization', request.headers.authorization)

      return response
    } catch (err) {
      return boom.unauthorized(errorMessage)
    }
  },  
  
  async getAll (request, h) {
    try {
      const users = await User.find()

      const data = users.map(convertToJsonSpecUser)
      const response = {
        data
      }

      return response
    } catch (err) {
      console.log(err)
      const errorMessage = 'Could not list all existing users'
      return boom.internal(errorMessage)
    }
  },

   async getOne (request, h) {
    try {
      const { idUser } = request.params

      const user = await User.findById(idUser)

      if (!user) {
        const errorMessage = 'This user does not exists'
        return boom.notFound(errorMessage)
      }

      const data = convertToJsonSpecUser(user)
      const response = {
        data
      }

      return response
    } catch (err) {
      const errorMessage = 'Could not list this user'
      return boom.badRequest(errorMessage)
    }
  },

  async getDoctors (request, h) {
    try {
      const doctors = await User.find({ 'typeuser': 'doctor' })
      const data = doctors.map(convertToJsonSpecUser)
      const response = {
        data
      }

      return response
    } catch (err) {
      const errorMessage = 'no doctors available'
      return boom.internal(errorMessage)
    }
  }
}
