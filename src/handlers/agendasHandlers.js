const boom = require('@hapi/boom')

const Agenda = require('../models/Agenda')


const convertToJsonSpec = require('../utils/convertToJsonSpec')

const nodemailer = require('nodemailer');

module.exports = {
  async getAll(request, h) {
    try {
      const agendas = await Agenda.find()

      const data = agendas.map(convertToJsonSpec)
      const response = {
        data
      }

      return response
    } catch (err) {
      console.log(err)
      const errorMessage = 'Could not list all existing agendas'
      return boom.internal(errorMessage)
    }
  },
  async getOne(request, h) {
    try {
      const { idAgenda } = request.params

      const agenda = await Agenda.findById(idAgenda)

      if (!agenda) {
        const errorMessage = 'This agenda does not exists'
        return boom.notFound(errorMessage)
      }

      const data = convertToJsonSpec(agenda)
      const response = {
        data
      }

      return response
    } catch (err) {
      const errorMessage = 'Could not list this agenda'
      return boom.badRequest(errorMessage)
    }
  },
  async create(request, h) {
    try {
      const agendaExists = await Agenda.findOne({ title: request.payload.title })
      if (agendaExists) {
        const errorMessage = 'This agenda already exists'
        return boom.conflict(errorMessage)
      }
      const agenda = await Agenda.create(request.payload)
      const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
          user: 'williandersonsanchez@gmail.com',
          pass: 'Wil 1996'
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      const data = convertToJsonSpec(agenda)
      const response = {
        data
      },
        contentHTML = `
         <h1>Resumen de la cita</h1>
          ${agenda}</li>
    `;
      const info = await transporter.sendMail({
        from: "'Servidor'",
        to: 'williandersonsanchez@gmail.com',
        subject: 'Informacion de la cita',
        html: contentHTML
      })
      return h.response(response).code(201)
    } catch (err) {
      const errorMessage = 'Could not create a new agenda'
      return boom.forbidden(errorMessage)
    }
  },
  async update(request, h) {
    try {
      const { idAgenda } = request.params
      const options = { new: true }

      const agendaDoesNotExists = !(await Agenda.findById(idAgenda))

      if (agendaDoesNotExists) {
        const errorMessage = 'This agenda does not exists'
        return boom.notFound(errorMessage)
      }

      const agenda = await Agenda.findByIdAndUpdate(idAgenda, request.payload, options)

      const data = convertToJsonSpec(agenda)
      const response = {
        data
      }

      return h.response(response)
    } catch (err) {
      const errorMessage = 'Could not update this agenda'
      return boom.forbidden(errorMessage)
    }
  },
  async delete(request, h) {
    try {
      const { idAgenda } = request.params

      const agendaDoesNotExists = !(await Agenda.findById(idAgenda))

      if (agendaDoesNotExists) {
        const errorMessage = 'This agenda does not exists'
        return boom.notFound(errorMessage)
      }

      await Agenda.findByIdAndDelete(idAgenda)

      return h.response().code(204)
    } catch (err) {
      const errorMessage = 'Could not delete this agenda'
      return boom.forbidden(errorMessage)
    }
  },
  async getAgendaPatient(request, h) {
    try {
      const { patient } = request.params

      const agenda = await Agenda.find({ patient: { $all: [patient] } })

      if (!agenda) {
        const errorMessage = 'The patient has no agendas'
        return boom.notFound(errorMessage)
      }

      const data = agenda.map(convertToJsonSpec)
      const response = {
        data
      }

      return response
    } catch (err) {
      const errorMessage = 'Could not list this agenda'
      return boom.badRequest(errorMessage)
    }
  }
}