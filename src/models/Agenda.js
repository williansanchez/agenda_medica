const mongoose = require('mongoose')

const AgendaSchema = new mongoose.Schema({
  title : {
    type: String,
    required: true,
    index: true,
    unique: true
  },
  description: {
    type: String,
    required: true
  },
  patient: {
    type: String,
    required: true
  },
  doctor: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  notification: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Agenda', AgendaSchema)
