const agendasHandlers = require('./handlers/agendasHandlers')
const usersHandlers = require('./handlers/usersHandlers')

const userSchema = require('./schemas/userSchema')
const {
  agendaSchema,
  agendaSchemaRequired
} = require('./schemas/agendaSchema')

module.exports = [
  /** users **/
  {
    method: 'POST',
    path: '/register',
    handler: usersHandlers.register,
    options: {
      validate: {
        payload: userSchema
      },
      auth: false
    }
  },
  {
    method: 'POST',
    path: '/login',
    handler: usersHandlers.login,
    options: {
      auth: false
    }
  },
  {
    method: 'GET',
    path: '/users',
    handler: usersHandlers.getAll,
    options: {
      auth: 'jwt'
    }
  },
  {
    method: 'GET',
    path: '/users/{idUser}',
    handler: usersHandlers.getOne,
    options: {
      auth: 'jwt'
    }
  },
  {
    method: 'GET',
    path: '/doctors',
    handler: usersHandlers.getDoctors,
    options: {
      auth: 'jwt'
    }
  },
  /** agendas **/
  {
    method: 'GET',
    path: '/agendas',
    handler: agendasHandlers.getAll,
    options: {
      auth: 'jwt'
    }
  },
  {
    method: 'GET',
    path: '/agendas/{idAgenda}',
    handler: agendasHandlers.getOne,
    options: {
      auth: 'jwt'
    }
  },
  {
    method: 'GET',
    path: '/agendasPatient/{patient}',
    handler: agendasHandlers.getAgendaPatient,
    options: {
      auth: 'jwt'
    }
  },
  {
    method: 'POST',
    path: '/agendas',
    handler: agendasHandlers.create,
    options: {
      validate: {
        payload: agendaSchemaRequired
      },
      auth: 'jwt'
    }
  },
  {
    method: 'PATCH',
    path: '/agendas/{idAgenda}',
    handler: agendasHandlers.update,
    options: {
      validate: {
        payload: agendaSchema
      },
      auth: 'jwt'
    }
  },
  {
    method: 'DELETE',
    path: '/agendas/{idAgenda}',
    handler: agendasHandlers.delete,
    options: {
      auth: 'jwt'
    }
  }
]
