module.exports = ( user) => {
  const { id, username, fullname, typeuser, email} = user
  const response = {
    type: 'users',
    id,
    attributes: {
      username,
      fullname,
      typeuser,
      email
    },
    links: {
      self: `/users/${id}`
    }
  }
 
  return response
}
