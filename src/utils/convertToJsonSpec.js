module.exports = (agenda) => {
  const { id, title, description, patient, doctor, date, notification } = agenda
  const response = {
    type: 'agendas',
    id,
    attributes: {
      title,
      description,
      patient,
      doctor,
      date,
      notification
    },
    links: {
      self: `/agendas/${id}`
    }
  }
  
  return response
}
